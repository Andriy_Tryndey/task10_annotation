package com.andriy.view;

import com.andriy.anotations.CarInfo;
import com.andriy.model.Car;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView(){
        menu = new LinkedHashMap<>();

        menu.put("1", "1. Create your own annotation. Create class with a few fields, some of which annotate with this annotation. Through reflection print those fields in the class that were annotate by this annotation.");
        menu.put("2", "2. Print annotation value into console (e.g. @Annotation(name ='111'))");
        menu.put("3", "3. Invoke method (three method with different parameters and return types)");
        menu.put("4", "4. Set value into field not knowing its type.Invoke myMethod(String a, int ... args) and myMethod(String … args).");
        menu.put("5", "5. Create your own class that received object of unknown type and show all information about that Class.");
        menu.put("Q", "Q - exit");

        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    /**
     * reaction to the first menu item
     */
    private void pressButton1() {
        Class clazz = Car.class;
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            if (declaredField.isAnnotationPresent(CarInfo.class)){
                CarInfo annotation = declaredField.getAnnotation(CarInfo.class);
                String model = annotation.model();
                double power = annotation.power();
                System.out.println("declaredField : " + declaredField.getName());
                System.out.println("Model in @CarInfo: " + model);
                System.out.println("Power in @CarInfo: " + power);
            }
        }
    }

    /**
     * reaction to the second menu item
     */
    private void pressButton2() {
        Class clazz = Car.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            System.out.println("Anotation : " + Arrays.toString(field.getAnnotations()));
        }
    }

    /**
     * reaction to the third menu item
     */
    private void pressButton3() {
        Car car = new Car();
        Class clazz = car.getClass();
        try {
            Method methodprint1 = clazz.getDeclaredMethod("print");
            methodprint1.invoke(car);
        }catch (Exception e){}

        String[] strs = {"one", "two", "three"};
        try {
            Method methodprint2 = clazz.getDeclaredMethod("print", new Class[]{String.class, String[].class});
            methodprint2.setAccessible(true);
            String[] hi = (String[]) methodprint2.invoke(car, "Hi", strs);
            for (String st: hi){
                System.out.println(st);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        try {
            Method methodprint3 = clazz.getDeclaredMethod("print", int.class, int.class);
            methodprint3.setAccessible(true);
            Integer printInt = (Integer) methodprint3.invoke(car,5, 7);
            System.out.println(printInt);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }


    }

    /**
     * response to the fourth menu item
     */
    private void pressButton4() {
        Car car = new Car();
        addParameter(car);
        System.out.println(car.getModel());
    }
    void addParameter(Object object){
        Class clazz = object.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            System.out.println("field : " + field.getName());
        }
        fields[0].setAccessible(true);
        System.out.println(fields[0].getType());
        if (fields[0].getType() == String.class){
            try {
                fields[0].set(object, "NISSAN");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * response to the fifrth menu item
     */
    private void pressButton5() {

    }

    private void outputMenu() {
        System.out.println("\nMENU");
        for (String str: menu.values()){
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));

    }
}
