package com.andriy.model;

import com.andriy.anotations.CarInfo;

import java.util.Objects;

/**
 * Create class Car
 */
public class Car {
    @CarInfo
    private String model;
    private String color;
    @CarInfo(power = 1.6)
    private double power;
    private int graduationYear;

    /**
     * constructor without parameters
     */
    public Car() {
    }

    /**
     * constructor with parameters
     * @param model
     * @param color
     * @param power
     * @param graduationYear
     */
    public Car(String model, String color, double power, int graduationYear) {
        this.model = model;
        this.color = color;
        this.power = power;
        this.graduationYear = graduationYear;
    }

    public String getModel() {
        return model;
    }

    public Car setModel(String model) {
        this.model = model;
        return this;
    }

    public String getColor() {
        return color;
    }

    public Car setColor(String color) {
        this.color = color;
        return this;
    }

    public double getPower() {
        return power;
    }

    public Car setPower(double power) {
        this.power = power;
        return this;
    }

    public int getGraduationYear() {
        return graduationYear;
    }

    public Car setGraduationYear(int graduationYear) {
        this.graduationYear = graduationYear;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Double.compare(car.power, power) == 0 &&
                graduationYear == car.graduationYear &&
                Objects.equals(model, car.model) &&
                Objects.equals(color, car.color);
    }

    @Override
    public int hashCode() {

        return Objects.hash(model, color, power, graduationYear);
    }

    public void print(){
        System.out.println("Method print.");
    }

    public String[] print(String str, String...args){
        String[] strings = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            strings[i] = str + "--" + args[i] + "--" + i;
        }
        return strings;
    }

    public int print(int x, int y){
        return x+y;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", power=" + power +
                ", graduationYear=" + graduationYear +
                '}';
    }
}
